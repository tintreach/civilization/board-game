
set(LIBRARY_NAME server_handle)

add_library(${LIBRARY_NAME} SHARED
        src/tcp_handle.cpp)
add_library(${PROJECT_NAME}::${LIBRARY_NAME} ALIAS ${LIBRARY_NAME})

target_link_libraries(${LIBRARY_NAME} PUBLIC utilities::utilities)

target_include_directories(${LIBRARY_NAME}
        INTERFACE
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        PRIVATE
        $<INSTALL_INTERFACE:include/${LIBRARY_NAME}>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/${LIBRARY_NAME}>)


set(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include/${LIBRARY_NAME})

set(HEADERS
        ${INCLUDE_DIR}/tcp_handle.h)
set_target_properties(${LIBRARY_NAME} PROPERTIES
        PUBLIC_HEADER "${HEADERS}")

set(EXPORT ${LIBRARY_NAME}-export)
install(TARGETS ${LIBRARY_NAME}
        EXPORT ${EXPORT}
        PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_PREFIX}/include/${LIBRARY_NAME}
        LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/${LIBRARY_NAME})

string(TOUPPER ${LIBRARY_NAME} LIBRARY_NAME_UPPER)
target_compile_definitions(${LIBRARY_NAME} PUBLIC ${LIBRARY_NAME_UPPER}_VERSION=${LIBRARY_VERSION})

if(${NATIVE})
    target_compile_definitions(${LIBRARY_NAME} PRIVATE NATIVE)
endif()