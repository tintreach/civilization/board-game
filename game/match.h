
#include "environment.h"

using namespace std;

namespace tintreach::board_game {

    class Object;

    class Character;

    class Vehicle;

    class Infrastructure;

    class Inventory;

    /*
        the distribution of all
    */
    class Layout;

    /*

    */
    class Object {
    private:
        int id;
        Letter letter;
        int owner_id;
    public:
        /*
            @returns id
        */
        int get_id();
    };

    /*

    */
    class Character : Object {
    private:


    };

    /*

    */
    class Vehicle : Object {


    };

    /*

    */
    class Infrastructure : Object {

    };

    class Layout {
    private:
        Object objects[];
    };
}