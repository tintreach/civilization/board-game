
#include "environment.h"

namespace tintreach::board_game {


    const Letter Location::get_letter() const {

        return this->letter;
    }
    
    const int Location::get_index() const {

        return this->index;
    }

    int Cell::get_level(){

        return level;
    }

    int Environment::location_to_index(const Location& location) {

        int index = 0;
        Letter curent_letter = Letter::A;
        int lenght = MIN_WIDTH;

        while(curent_letter < location.get_letter()){
            
            curent_letter = (Letter)((int)curent_letter + 1);
            index += lenght;
            if(curent_letter < Letter::O)
                lenght ++;
            else
                lenght --;
        }

        index += location.get_index();

        return index;
    }

    int Environment::get_level(Location location) {

        Cell* cell = &this->cells[this->location_to_index(location)];

        return cell->get_level();
    }
}