
#define CELL_COUNT 631
#define MIN_WIDTH 15
#define MAX_WIDTH 29

namespace tintreach::board_game {

    enum Letter;

    /* Cell location

    */
    struct Location;
    
    class Cell;

    class Environment;

    enum Letter {
        A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, AA, BB, CC
    };

    /*
    
    */
    struct Location {
    private:
        const Letter letter;
        const int index;
    public:
        const Letter get_letter() const;
        const int get_index() const;
    };

    class Cell {
    private:
        /*
            @warning level 0 equal ocean level 
        */
        int level;
    public:
        int get_level();
    };

    class Environment {
    private:
        Cell cells[CELL_COUNT];

        int location_to_index(const Location& location);
    public:
        int get_level(Location);
    };
}