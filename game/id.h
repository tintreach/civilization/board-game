

namespace tintreach::board_game {

    /*
        @brief unique identification of game
    */
    class ID;

    class ID {
    private:
        /* 
            @brief index of production
            @example id=4=forth game produced
        */
        int id;
    };
}